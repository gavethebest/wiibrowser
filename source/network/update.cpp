/****************************************************************************
 * libwiigui Template
 * Tantric 2009
 * modified by gave92
 *
 * WiiBrowser
 * update.cpp
 * Wii/GameCube auto-update management
 ***************************************************************************/

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "common.h"
#include "config.h"
#include "transfer.h"

int readFile(FILE *hfile, struct update *result)
{
    char line[512];
    int new_v;
    int gettext = -1;

    fscanf (hfile, "APPVERSION: R%d", &new_v);

	while (fgets(line, sizeof(line), hfile))
	{
        if(gettext >= 0)
        {
            strcpy(result->changelog+gettext, line);
            gettext = strlen(result->changelog);
        }

		else if (!strncmp(line, "# Changelog", 11))
            gettext = 0;
	}
    return new_v;
}

/*
bool replaceInFile()
{
    FILE *fp = fopen("sd:/apps/wiibrowser/boot.dol", "rb+");
    if(!fp)
        return false;

#ifdef MPLAYER
    long res = 13683343;
#else
    long res = 7002879;
#endif
    char buffer[] = { '1' };

    fseek(fp, res, SEEK_SET);
    fwrite(buffer, sizeof(char), 1, fp);

    fclose(fp);
    return true;
}
*/

bool downloadUpdate(int appversion) {
	char updateFile[30];
	sprintf(updateFile, "update.dol");

    bool result = false;
    char url[100];

    if (Settings.Autoupdate == STABLE)
        sprintf(url, "http://wiibrowser.altervista.org/tests/wii-update.php?file=R%d.dol&uuid=%s&update=1", appversion, Settings.Uuid);
    else sprintf(url, "https://dl.dropbox.com/s/w39ycq91i3wwvn5/boot.dol?dl=1");

	Private *data = NULL;
    FILE *hfile = fopen(updateFile, "wb");

	if (hfile)
	{
	    data = AddUpdate(curl_multi, url, hfile);
        while(data->code < 0)
            usleep(100);

		if(!data->code)
		{
		    remove("boot.dol");
		    result = (rename(updateFile, "boot.dol")==0);
		}
		if(data->code || !result)
		{
            remove(updateFile); // delete update file
		}
	}
	if (result)
	{
	    Settings.RevInt = appversion;
	    Settings.Save(0);
	}

	free(data->url);
	delete(data);
	return result;
}

struct update checkUpdate() {
	char updateFile[30];
	sprintf(updateFile, "update.cfg");
    char url[70];

    struct update result;
    struct block HTML;
    result.appversion = 0;

/*
    if (Settings.Autoupdate == STABLE)
        sprintf(url, "http://wiibrowser.altervista.org/downloads/wiibrowser.cfg");
    else sprintf(url, "https://dl.dropbox.com/s/acpbajdid91d7it/nightly.cfg?dl=1");
*/

    if (Settings.Autoupdate == STABLE)
        sprintf(url, "http://wiibrowser.altervista.org/downloads/wiibrowser_.cfg");
    else sprintf(url, "https://dl.dropbox.com/s/acpbajdid91d7it/nightly.cfg?dl=1");

    CURL *curl_upd = curl_easy_init();
	FILE *hfile = fopen(updateFile, "wb");

	if (hfile)
	{
	    HTML = downloadfile(curl_upd, url, hfile);
		if(HTML.size)
		{
            int old_v, new_v;
            hfile = fopen(updateFile, "r");
            old_v = Settings.RevInt;
            new_v = readFile(hfile, &result);

            if (new_v>old_v)
                result.appversion = new_v;
		}
        fclose(hfile);
        remove(updateFile); // delete update file
	}

	curl_easy_cleanup(curl_upd);
	return result;
}

bool get_mac(char *macStr)
{
    u8 mac_buf[6];
    if(net_get_mac_address(mac_buf))
        return false;

    snprintf(macStr, 13, "%02x%02x%02x%02x%02x%02x",
         mac_buf[0], mac_buf[1], mac_buf[2], mac_buf[3], mac_buf[4], mac_buf[5]);
    return true;
}

bool secureUuid()
{
    bool result = false;
    char url[128];
    char macStr[13];

    if(!get_mac((macStr)))
        return result;

    CURL *curl_upd = curl_easy_init();
    sprintf(url, "http://wiibrowser.altervista.org/tests/secure-uuid.php?file=R%d.dol&mac=%s&uuid=%s", Settings.RevInt, macStr, Settings.Uuid);
    struct block HTML = downloadfile(curl_upd, url, NULL);

    if(HTML.size > 0)
    {
        if(!strcmp(HTML.data, "VALID"))
            result = true;
    }

    free(HTML.data);
    curl_easy_cleanup(curl_upd);
	return result;
}
