#include "main.h"
#include "networkop.h"
#include "ruleset.h"
#include "settings.h"

extern "C" {
#include "urlcode.h"
}

u8 networkstack[GUITH_STACK] ATTRIBUTE_ALIGN (32);
lwp_t networkthread = LWP_THREAD_NULL;

int networkThreadHalt = 0;
bool networkinit = 1;
extern list<string> suggestions;

/****************************************************************************
 * NetworkThread
 *
 * Thread to handle connection.
 ***************************************************************************/
void *NetworkThread (void *arg)
{
    s32 res = -1;
	int retry;
	int wait;
	static bool prevInit = false;

	while(!networkThreadHalt)
	{
		retry = 5;

		while (retry > 0 && !networkThreadHalt)
		{
			net_deinit();

			if(prevInit)
			{
				prevInit = false; // only call net_wc24cleanup once
				net_wc24cleanup(); // kill wc24
				usleep(10000);
			}

			res = net_init_async(NULL, NULL);

			if(res != 0)
			{
				sleep(1);
				retry--;
				continue;
			}

			res = net_get_status();
			wait = 500; // only wait 10 sec

			while (res == -EBUSY && wait > 0 && !networkThreadHalt)
			{
				usleep(20*1000);
				res = net_get_status();
				wait--;
			}

			if (res == 0)
			{
				// struct in_addr hostip;
				// hostip.s_addr = net_gethostip();

				if (CheckConnection())
				{
					networkinit = true;
					prevInit = true;
					break;
				}
			}

			retry--;
			usleep(2000);
		}

		if(!networkThreadHalt)
            LWP_SuspendThread(networkthread);
	}
	return NULL;
}

void InitNetwork()
{
    networkThreadHalt = 0;
    networkinit = 0;

	if(networkthread == LWP_THREAD_NULL)
		LWP_CreateThread(&networkthread, NetworkThread, NULL, networkstack, GUITH_STACK, 30);
    else LWP_ResumeThread(networkthread);
}

void StopNetwork()
{
    if(networkthread == LWP_THREAD_NULL)
        return;

    networkThreadHalt = 1;
    LWP_ResumeThread(networkthread);

    LWP_JoinThread(networkthread, NULL);
    networkthread = LWP_THREAD_NULL;
}

bool CheckConnection()
{
    s32 s = net_socket (AF_INET, SOCK_STREAM, IPPROTO_IP);
    struct sockaddr_in sa;

    if(s < 0)
        return false;

    memset (&sa, 0, sizeof (struct sockaddr_in));
    sa.sin_family = AF_INET;
    sa.sin_len = sizeof (struct sockaddr_in);
    sa.sin_port = htons(80);
    inet_aton("216.127.94.127", &sa.sin_addr); // store IP in sa

    int res = net_connect (s, (struct sockaddr *) &sa, sizeof (struct sockaddr_in));
    net_close (s);

    return (res >= 0);
}

Private *CheckGoogleQueries(char *text)
{
    string url = "http://suggestqueries.google.com/complete/search?client=firefox&hl=";
    char *encode = url_encode(text);

    url += LangName[Settings.Language-1];
    url += "&q=";
    url += encode;

    free(encode);
    return AddGoogleQuery(curl_multi, (char *)url.c_str());
}

void CompleteQuery(Private *data)
{
    int substr_b, substr_e;
    int span;
    string ext, word;

    if (!data->mem.size)
    {
        free(data->url);
        free(data->mem.memory);
        delete(data);
        return;
    }

    word.assign(data->mem.memory);
    substr_b = word.find(",[\"", 0) + 2;

    char delim = word.at(substr_b);
    char *text = url_decode(strrchr(data->url, '=') + 1);

    while((substr_b++) != string::npos)
    {
        substr_e = word.find(delim, substr_b);
        span = substr_e - substr_b;
        ext = word.substr(substr_b, span);

        if(!strncasecmp(text, ext.c_str(), strlen(text)))
            suggestions.push_back(ext);

        substr_b = word.find(delim, substr_e + 1);
    }

    free(data->url);
    free(data->mem.memory);
    free(text);
    delete(data);
}

void DownloadFile(string url, bool last)
{
    file hfile;
    hfile.file = NULL;

    string fnd, content;
    struct block HTML;
    CURL *handle = curl_easy_init();

    if(handle)
    {
        if(!strncasecmp(url.c_str(), "ftp", 3))
        {
            HTML.data = NULL;
            HTML.size = -1;
            memset(HTML.type, 0, sizeof(HTML.type));
        }
        else HTML = downloadfile(handle, url.c_str(), NULL);

        /* mediafire downloads */
        if(HTML.size > 0)
        {
            content = HTML.data;
            free(HTML.data);
            HTML.data = NULL;

            if(!strncmp(url.c_str(), "http://www.mediafire.com/download/", 34))
            {
                fnd = get_mediafire(&content);
                if(fnd.size() > 0)
                {
                    url.assign(fnd);
                    HTML.size = -1;
                }
            }
        }

        if(HTML.size == -1)
        {
            if(performDownload(&hfile, (char *)url.c_str(), &HTML))
                PushQueue(curl_multi, (char *)url.c_str(), &hfile, 0, last);
        }
    }

    if(last)
        LWP_ResumeThread(downloadthread);

    curl_easy_cleanup(handle);
}
