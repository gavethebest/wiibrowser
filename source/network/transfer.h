#ifndef __TRANSFER_H__
#define __TRANSFER_H__

#include <unistd.h>
#include <network.h>

#include "common.h"
#include "httplib.h"
#include "menu.h"

#define GUITH_STACK 	(16384)
#define DLTH_STACK      (131072)
#define MAXD            6

typedef struct data
{
    char *url;
    int *bar;
    file save;
    memory mem;
    int code;
    double bytes;
    bool keep;
} Private;

void *DownloadThread (void *arg);
void StopDownload();

Private *AddUpdate(CURLM *cm, char *url, FILE *file);
Private *AddGoogleQuery(CURLM *cm, char *url);
Private *AddDownload(CURLM *cm, char *url, file *file);
Private *PushQueue(CURLM *cm, char *url, file *file, bool keep, bool resume = true);

extern GuiWindow *mainWindow;
extern GuiDownloadManager *manager;
extern GuiText *downloads[5];

extern lwp_t downloadthread;
extern u8 downloadstack[DLTH_STACK];

#endif
