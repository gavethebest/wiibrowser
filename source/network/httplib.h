/*
**  Copyright (C) 2012
**  ==================
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, version 2.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _HTTPLIB_H_
#define _HTTPLIB_H_

#include <network.h>
#include <string.h>
#include <curl/curl.h>

// -----------------------------------------------------------
// DEFINES
// -----------------------------------------------------------

#define MAX_LEN         256
#define TYPE            50
#define DEBUG_LEVEL
#define APPNAME         "WiiBrowser"

typedef struct MemoryStruct {
    char *memory;
    u32 size;
} memory;

typedef struct block
{
    char *data;
    int size;
    char type[TYPE];
} block;

extern const struct block emptyblock;

bool postcomment(CURL *curl_handle, char *name, char *content);
struct block getrequest(CURL *curl_handle, const char *url, FILE *hfile, int checkdl = 0);
struct block downloadfile(CURL *curl_handle, const char *url, FILE *hfile);

size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);

void save(struct block *b, FILE *hfile);
bool validProxy();

char *findChr (const char *str, char chr);
char *findRchr (const char *str, char chr);

void DebugInt(u32 msg);
void Debug(const char *msg);

#endif
